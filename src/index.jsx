import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import './App.css';

import reportWebVitals from './reportWebVitals';
import { BrowserRouter, useHistory } from "react-router-dom";
import { Auth0Provider } from '@auth0/auth0-react';
import { clientID, domain, redirectUri } from './config';
import { GlobalProvider } from './context/Provider';
import { App } from './App';
import { CaretLeftOutlined } from '@ant-design/icons';
ReactDOM.render(    

  <GlobalProvider>

      <BrowserRouter>
      <Auth0Provider
        domain={domain}
        clientId={clientID}
        redirectUri={redirectUri}
      >
          <div style={{paddingBottom:'30px'}}>
            <div className='dragger'>
            <div ><CaretLeftOutlined  onClick={()=>useHistory().goBack()}  style={{color:'white'}} />  </div>
              </div>
              </div>
      <App/>

      </Auth0Provider> 

      </BrowserRouter>
      </GlobalProvider>
,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(// // // console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
