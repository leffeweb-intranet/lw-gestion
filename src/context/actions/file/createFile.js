import {  CREATE_FILE_ERROR, CREATE_FILE_LOAD, CREATE_FILE_SUCCESS, CREATE_MESSAGE_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";
import axios from "axios";

export const createFile= (values) => (dispatch)=>{
  
    dispatch({
        type: CREATE_FILE_LOAD,
    })
    axios({
        method: 'post',
        url: `http://localhost:5000/api/file/upload`,
        data: values,
        headers: { 'Content-Type': 'multipart/form-data' },
      })   
      .then((res) => {
        dispatch({
            type: CREATE_FILE_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: CREATE_FILE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}