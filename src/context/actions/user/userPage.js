import { SUPER_USER_ERROR, SUPER_USER_LOADING, SUPER_USER_PAGE_ERROR, SUPER_USER_PAGE_LOADING, SUPER_USER_PAGE_SUCCESS, SUPER_USER_SUCCESS, USER_ERROR, USER_LOADING, USER_PAGE_ERROR, USER_PAGE_LOADING, USER_PAGE_SUCCESS, USER_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const userPage = (userID) => (dispatch) =>{

    dispatch({
        type: SUPER_USER_PAGE_LOADING,
    })
    
    axiosInstance()
    .get(`client/admin/client/${userID}`)
    .then(res => {
        console.log(res);
        dispatch({
            type: SUPER_USER_PAGE_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: SUPER_USER_PAGE_ERROR,
            payload: err.response?.data || CONNECTION_ERROR  ,
        })
    })
}