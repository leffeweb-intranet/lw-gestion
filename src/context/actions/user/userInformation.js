import { useHistory } from "react-router";
import { USER_ERROR, USER_LOADING, USER_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const userInformation = (userID) => (dispatch) =>{
    dispatch({
        type: USER_LOADING,
    })
    
    axiosInstance()
    .get(`/client/${userID}`)
    .then(res => {
        console.log("res",res);
       
        dispatch({
            type: USER_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: USER_ERROR,
            payload: err.response?.data || CONNECTION_ERROR  ,
        })
    })
}