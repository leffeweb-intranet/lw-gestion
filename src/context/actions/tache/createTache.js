import {  CREATE_TACHE_ERROR, CREATE_TACHE_LOAD, CREATE_TACHE_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const createTache= (values) => (dispatch)=>{
    // // console.log("values",values);
    dispatch({
        type: CREATE_TACHE_LOAD,
    })

    axiosInstance()
    .post(`/tache`,values)
    .then((res) => {
        // // console.log("res",res);
        dispatch({
            type: CREATE_TACHE_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        // // console.log("err",err);

        dispatch({
            type: CREATE_TACHE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}