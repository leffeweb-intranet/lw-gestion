import { useContext } from "react";
import { ADD_QUESTION_ERROR, ADD_QUESTION_LOADING, ADD_QUESTION_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const createQuestion = (values) => (dispatch)=>{
    dispatch({
        type: ADD_QUESTION_LOADING,
    })

   
    values.userId= localStorage.getItem('userID');
    axiosInstance()
    .post("/question",values)
    .then((res) => {

        dispatch({
            type: ADD_QUESTION_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        // // console.log(err);

        dispatch({
            type: ADD_QUESTION_ERROR,
            payload: err ? (err?.response && err.response.data) : CONNECTION_ERROR,
        })
    })
}