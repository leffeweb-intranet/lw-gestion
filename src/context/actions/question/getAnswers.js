import { LIST_ANSWER_ERROR, LIST_ANSWER_LOADING, LIST_ANSWER_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getAnswers = ({id}) => (dispatch)=>{
    dispatch({
        type: LIST_ANSWER_LOADING,
    })

    axiosInstance()
    .get(`/answer/${id}`)
    .then((res) => {

        dispatch({
            type: LIST_ANSWER_SUCCESS,
            payload: res.data.answers,
        })

    })
    .catch(err => {
        dispatch({
            type: LIST_ANSWER_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}