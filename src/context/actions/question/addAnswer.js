import { useContext } from "react";
import { ADD_ANSWER_ERROR, ADD_ANSWER_LOADING, ADD_ANSWER_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const addAnswer = (values) => (dispatch)=>{
    dispatch({
        type: ADD_ANSWER_LOADING,
    })

   
    values.userId= localStorage.getItem('userID');
    axiosInstance()
    .post("/answer",values)
    .then((res) => {

        dispatch({
            type: ADD_ANSWER_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        // // console.log(err);

        dispatch({
            type: ADD_ANSWER_ERROR,
            payload: err ? (err?.response && err.response.data) : CONNECTION_ERROR,
        })
    })
}