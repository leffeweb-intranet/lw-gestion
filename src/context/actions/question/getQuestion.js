import { QUESTION_LOADING, QUESTION_LOADING_ERROR, QUESTION_LOADING_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getQuestion = ({id}) => (dispatch)=>{
    dispatch({
        type: QUESTION_LOADING,
    })

    axiosInstance()
    .get(`/question/${id}`)
    .then((res) => {
        // // console.log('res question', res);

        dispatch({
            type: QUESTION_LOADING_SUCCESS,
            payload: res.data.question,
        })

    })
    .catch(err => {
        dispatch({
            type: QUESTION_LOADING_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}