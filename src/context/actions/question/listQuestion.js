import {  LIST_QUESTION_LOADING, LIST_QUESTION_LOADING_ERROR, LIST_QUESTION_LOADING_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const listQuestion = () => (dispatch)=>{
    dispatch({
        type: LIST_QUESTION_LOADING,
    })

    axiosInstance()
    .get("/question")
    .then((res) => {

        dispatch({
            type: LIST_QUESTION_LOADING_SUCCESS,
            payload: res.data.questions,
        })

    })
    .catch(err => {
        dispatch({
            type: LIST_QUESTION_LOADING_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}