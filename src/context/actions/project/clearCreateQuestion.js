import { CLEAR_ADD_QUESTION } from "../../../constants/actionTypes"

export  default ()=> (dispatch)=>{
    dispatch({
        type:CLEAR_ADD_QUESTION
    })
}