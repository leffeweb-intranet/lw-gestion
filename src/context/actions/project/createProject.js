import {  CREATE_PROJECT_ERROR, CREATE_PROJECT_LOAD, CREATE_PROJECT_SUCCESS, LIST_PROJECT_ERROR, LIST_PROJECT_LOAD, LIST_PROJECT_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const createProject = (values) => (dispatch)=>{
    // // console.log("values",values);
    dispatch({
        type: CREATE_PROJECT_LOAD,
    })

    axiosInstance()
    .post(`/project/dez`,values)
    .then((res) => {
        // // console.log("res",res);
        dispatch({
            type: CREATE_PROJECT_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        // // console.log("err",err);

        dispatch({
            type: CREATE_PROJECT_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}