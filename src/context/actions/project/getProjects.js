import {  LIST_PROJECT_ERROR, LIST_PROJECT_LOAD, LIST_PROJECT_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getProjects = (id) => (dispatch)=>{
    dispatch({
        type: LIST_PROJECT_LOAD,
    })


    // // console.log("id",id);
    axiosInstance()
    .get(`/project/admin/client/${id}`)
    .then((res) => {
        // console.log("res",res);
        dispatch({
            type: LIST_PROJECT_SUCCES,
            payload: res.data,
        })

    })
    .catch(err => {
        // // console.log("err",err);

        dispatch({
            type: LIST_PROJECT_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}