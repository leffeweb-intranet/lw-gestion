import { PROJECT_ERROR, PROJECT_LOAD, PROJECT_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getProject = (id) => (dispatch)=>{
    console.log('id request',id);
    dispatch({
        type: PROJECT_LOAD,
    })

    axiosInstance()
    .get(`/project/${id}`)
    .then((res) => {
        dispatch({
            type: PROJECT_SUCCES,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: PROJECT_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}