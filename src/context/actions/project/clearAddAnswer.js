import { CLEAR_ADD_ANSWER } from "../../../constants/actionTypes"

export  default ()=> (dispatch)=>{
    dispatch({
        type:CLEAR_ADD_ANSWER
    })
}