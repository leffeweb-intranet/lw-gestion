import { LIST_MODULE_ERROR, LIST_MODULE_LOADING, LIST_MODULE_SUCCESS, MODULE_ERROR, MODULE_LOADING, MODULE_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const module = (history) => (dispatch) =>{

    dispatch({
        type: MODULE_LOADING,
    })
    

    axiosInstance(history)
    .get(`/appmodule`)
    .then(res => {
        dispatch({
            type: MODULE_SUCCESS,
            payload: res.data.module,
        })
    })
    .catch(err => {
        dispatch({
            type: MODULE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}