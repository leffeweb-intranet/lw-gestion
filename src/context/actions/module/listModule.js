import { LIST_MODULE_ERROR, LIST_MODULE_LOADING, LIST_MODULE_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const listModule = (history) => (dispatch) =>{

    dispatch({
        type: LIST_MODULE_LOADING,
    })
    

    axiosInstance(history)
    .get(`/appmodule`)
    .then(res => {
        dispatch({
            type: LIST_MODULE_SUCCESS,
            payload: res.data.appModules,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_MODULE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}