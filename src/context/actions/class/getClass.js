import { LIST_CLASS_ERROR, LIST_CLASS_LOADING, LIST_CLASS_SUCCESS } from "../../../constants/actionTypes";
import { CONNECTION_ERROR } from "../../../constants/api";
import axiosInstance from "../../../helpers/axiosInstance";


export const listClass = (history) => (dispatch) =>{

    dispatch({
        type: LIST_CLASS_LOADING,
    })
    

    axiosInstance(history)
    .get(`/appClass`)
    .then(res => {
        dispatch({
            type: LIST_CLASS_SUCCESS,
            payload: res.data.appsClass,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_CLASS_ERROR,
            payload: err.response?.data || CONNECTION_ERROR  ,
        })
    })
}