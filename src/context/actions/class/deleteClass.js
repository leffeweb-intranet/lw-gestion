import { useContext } from "react";
import { DELETE_CLASS_ERROR, DELETE_CLASS_LOADING, DELETE_CLASS_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const deleteClass = (classID) => (dispatch)=>{

    dispatch({
        type: DELETE_CLASS_LOADING,
    })

    axiosInstance()
    .delete(`/appClass/${classID}`)
    .then((res) => {
        dispatch({
            type: DELETE_CLASS_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: DELETE_CLASS_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}