import { useContext } from "react";
import { ADD_CLASS_ERROR, ADD_CLASS_LOADING, ADD_CLASS_SUCCESS, ADD_QUESTION_ERROR, ADD_QUESTION_LOADING, ADD_QUESTION_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const addClass = (values) => (dispatch)=>{

    dispatch({
        type: ADD_CLASS_LOADING,
    })

    axiosInstance()
    .post("/appClass",values)
    .then((res) => {
        dispatch({
            type: ADD_CLASS_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: ADD_CLASS_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}