import { CLEAR_MESSAGE } from "../../../constants/actionTypes"

export  default ()=> (dispatch)=>{
    dispatch({
        type:CLEAR_MESSAGE
    })
}