import {  CREATE_MESSAGE_ERROR, CREATE_MESSAGE_LOAD, CREATE_MESSAGE_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const createMessage= (values) => (dispatch)=>{
    console.log("createmessage",values);
    dispatch({
        type: CREATE_MESSAGE_LOAD,
    })
    axiosInstance()
    .post(`/message`,values)
    .then((res) => {
        dispatch({
            type: CREATE_MESSAGE_SUCCESS,
            payload: res.data,
        })

    })
    .catch(err => {
        dispatch({
            type: CREATE_MESSAGE_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}