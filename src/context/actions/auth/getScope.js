import { useEffect } from "react";


export const getToken = async (scope,getAccessTokenSilently,getAccessTokenWithPopup) => {

  if(localStorage.getItem('token')) return localStorage.getItem('token')

  try {
    const token = await getAccessTokenSilently({
      audience: 'http://localhost:5000/api',
      scope: scope,     
      });
      console.log(token);
      localStorage.setItem('token',token)
  } catch (e) {
    //   try { 
    //   const token = await getAccessTokenWithPopup({
    //     audience: 'http://localhost:5000/api',
    //     scope: scope,     
    //     });
    //     console.log(token);
    //     localStorage.setItem('token',token)
    //   } catch (e) {
    //     console.error(e);
        
    // }
    console.log(e);
}
   return localStorage.getItem('token')
}