import { REGISTER_ERROR, REGISTER_LOADING, REGISTER_SUCCESS } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";


export const register = (values) => (dispatch) =>{

    dispatch({
        type: REGISTER_LOADING,

    })
    
    axiosInstance()
    .post("/users",values)
    .then(res => {
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: REGISTER_ERROR,
            payload: err.response.data,
        })
    })
}