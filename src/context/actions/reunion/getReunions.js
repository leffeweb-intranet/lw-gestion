import { LIST_REUNION_ERROR, LIST_REUNION_LOAD, LIST_REUNION_SUCCES } from "../../../constants/actionTypes";
import axiosInstance from "../../../helpers/axiosInstance";
import {CONNECTION_ERROR} from  "../../../constants/api";

export const getReunions = (id) => (dispatch)=>{
    dispatch({
        type: LIST_REUNION_LOAD,
    })

   axiosInstance()
    .get(`/reunion/${id}`)
    .then((res) => {
        dispatch({
            type: LIST_REUNION_SUCCES,
            payload: res.data,
        })
    })
    .catch(err => {
        dispatch({
            type: LIST_REUNION_ERROR,
            payload: err.response ? err.response.data : CONNECTION_ERROR,
        })
    })
}