import { CREATE_TACHE_ERROR, CREATE_TACHE_LOAD, CREATE_TACHE_SUCCESS, EDIT_TACHE_ERROR, EDIT_TACHE_LOAD, EDIT_TACHE_SUCCESS, LIST_TACHE_ERROR, LIST_TACHE_LOAD, LIST_TACHE_SUCCES, TACHE_ERROR, TACHE_LOAD, TACHE_SUCCES } from "../../constants/actionTypes";

const project=(state,{payload,type})=>{
    switch (type){
        case TACHE_LOAD:
            return {
                ...state,
                tache:{
                    ...state.tache,
                    loading: true,
                    error:null
                }
            }
           
        case TACHE_ERROR:
                return {
                    ...state,
                    tache:{
                        ...state.tache,
                        loading: false,
                        error:payload
                    }
                }

        case TACHE_SUCCES:
            return {
                ...state,
                tache:{
                  ...state.tache,
                   loading: false,
                   data: payload
                    }
                 }

        case LIST_TACHE_LOAD:
            return {
                ...state,
                taches:{
                    ...state.taches,
                    loading: true,
                    error:null
                }
            }
           
        case LIST_TACHE_ERROR:
                return {
                    ...state,
                    taches:{
                        ...state.taches,
                        loading: false,
                        error:payload
                    }
                }

        case LIST_TACHE_SUCCES:
            return {
                ...state,
                taches:{
                  ...state.taches,
                   loading: false,
                   data: payload
                    }
                 }

        case CREATE_TACHE_LOAD:
            return {
                ...state,
                addTache:{
                    ...state.addTache,
                    loading: true,
                    error:null
                }
            }            
       case CREATE_TACHE_ERROR:
                return {
                    ...state,
                    addTache:{
                        ...state.addTache,
                        loading: false,
                        error:payload
                    }
                }
    case CREATE_TACHE_SUCCESS:
            return {
                ...state,
                addTache:{
                  ...state.addTache,
                   loading: false,
                   data: payload
                    }
                 }

                 
    case EDIT_TACHE_LOAD:
            return {
                ...state,
                editTache:{
                    ...state.editTache,
                    loading: true,
                    error:null
                }
            }            
       case EDIT_TACHE_ERROR:
                return {
                    ...state,
                    editTache:{
                        ...state.editTache,
                        loading: false,
                        error:payload
                    }
                }
    case EDIT_TACHE_SUCCESS:
            return {
                ...state,
                editTache:{
                  ...state.editTache,
                   loading: false,
                   data: payload
                    }
                 }
                           
        default:
            return state;
    }
}

export default project;