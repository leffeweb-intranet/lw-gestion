import { CREATE_PROJECT_ERROR, CREATE_PROJECT_LOAD, CREATE_PROJECT_SUCCESS, LIST_PROJECT_ERROR,LIST_PROJECT_LOAD,LIST_PROJECT_SUCCES, PROJECT_ERROR, PROJECT_LOAD, PROJECT_SUCCES } from "../../constants/actionTypes";

const project=(state,{payload,type})=>{
    switch (type){
        case PROJECT_LOAD:
            return {
                ...state,
                project:{
                    ...state.project,
                    loading: true,
                    error:null
                }
            }
           
        case PROJECT_ERROR:
                return {
                    ...state,
                    project:{
                        ...state.project,
                        loading: false,
                        error:payload
                    }
                }

        case PROJECT_SUCCES:
            return {
                ...state,
                project:{
                  ...state.project,
                   loading: false,
                   data: payload
                    }
                 }

        case LIST_PROJECT_LOAD:
            return {
                ...state,
                projects:{
                    ...state.projects,
                    loading: true,
                    error:null
                }
            }
           
        case LIST_PROJECT_ERROR:
                return {
                    ...state,
                    projects:{
                        ...state.projects,
                        loading: false,
                        error:payload
                    }
                }

        case LIST_PROJECT_SUCCES:
            return {
                ...state,
                projects:{
                  ...state.projects,
                   loading: false,
                   data: payload
                    }
                 }

        case CREATE_PROJECT_LOAD:
            return {
                ...state,
                addProject:{
                    ...state.addProject,
                    loading: true,
                    error:null
                }
            }            
       case CREATE_PROJECT_ERROR:
                return {
                    ...state,
                    addProject:{
                        ...state.addProject,
                        loading: false,
                        error:payload
                    }
                }
    case CREATE_PROJECT_SUCCESS:
            return {
                ...state,
                addProject:{
                  ...state.addProject,
                   loading: false,
                   data: payload
                    }
                 }
                           
        default:
            return state;
    }
}

export default project;