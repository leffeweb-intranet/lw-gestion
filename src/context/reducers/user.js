import { CLEAR_EDIT_USER, USER_LOADING, USER_ERROR, EDIT_USER_LOADING, EDIT_USER_SUCCESS, USER_SUCCESS, LIST_USER_ERROR, LIST_USER_SUCCESS, LIST_USER_LOADING, USER_PAGE_LOADING, USER_PAGE_SUCCESS, SUPER_USER_PAGE_SUCCESS, SUPER_USER_PAGE_ERROR, SUPER_USER_PAGE_LOADING } from "../../constants/actionTypes";

const user=(state,{payload,type})=>{
    switch (type){
        case USER_LOADING:
            return {
                ...state,
                user:{
                    ...state.user,
                    loading: true,
                    error:null
                }
            }
           
        case USER_ERROR:
                return {
                    ...state,
                    user:{
                        ...state.user,
                        loading: false
                    }
                }
        case USER_SUCCESS:
            return {
                ...state,
                user:{
                  ...state.user,
                   loading: false,
                   data: payload
                    }
                 }



         case EDIT_USER_LOADING:
                return{
                    ...state,
                    editUser : {
                        ...state.editUser,
                        loading:true,
                        error:null
                    }
                }
        case EDIT_USER_SUCCESS:
                return {
                    ...state,
                    editUser:{
                        ...state.editUser,
                        loading: false,
                        data: payload
                    }
                }

                case LIST_USER_SUCCESS:
                    return {
                        ...state,
                        users:{
                            ...state.users,
                            loading: false,
                            data: payload
                        }
                    }

                    case LIST_USER_LOADING:
                        return {
                            ...state,
                            users:{
                                ...state.users,
                                loading: true,
                                error:null
                            }
                        }
                       
                    case LIST_USER_ERROR:
                            return {
                                ...state,
                                users:{
                                    ...state.users,
                                    loading: false
                                }
                            }
                            
                case CLEAR_EDIT_USER:{
                    return{
                        ...state,
                        editUser : {
                            ...state.editUser,
                            loading:false,
                            error:null,
                            data:null,
                        }
                    }
                }


                case SUPER_USER_PAGE_LOADING:
                    return {
                        ...state,
                        userpage:{
                            ...state.userpage,
                            loading: true,
                            error:null
                        }
                    }
                   
                case SUPER_USER_PAGE_ERROR:
                        return {
                            ...state,
                            userpage:{
                                ...state.userpage,
                                loading: false
                            }
                        }
                case SUPER_USER_PAGE_SUCCESS:
                    return {
                        ...state,
                        userpage:{
                          ...state.userpage,
                           loading: false,
                           data: payload
                            }
                         }

        default:
            return state;
    }
}

export default user;