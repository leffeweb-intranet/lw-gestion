import React,{ createContext,useReducer } from "react";

import project from "./reducers/project";
import user from "./reducers/user";
import tache from "./reducers/tache";
import reunion from "./reducers/reunion";
import message from "./reducers/message";
import file from "./reducers/file";

import projectInitialState from "../initialstates/projectInitialState";
import userInitialState from "../initialstates/userInitialState";
import tacheInitialState from "../initialstates/tacheInitialState";
import reunionInitialState from "../initialstates/reunionInitialState";
import messageInitialState from "../initialstates/messageInitialState";
import fileInitialState from "../initialstates/fileInitialState";


export const GlobalContext=createContext({});

export const GlobalProvider=({children})=>{

    const [projectState,projectDispatch] = useReducer(project,projectInitialState);
    const [userState,userDispatch] = useReducer(user,userInitialState);
    const [tacheState,tacheDispatch] = useReducer(tache,tacheInitialState);
    const [reunionState,reunionDispatch] = useReducer(reunion,reunionInitialState);
    const [messageState,messageDispatch] = useReducer(message,messageInitialState);
    const [fileState,fileDispatch] = useReducer(file,fileInitialState);

    return(
    <GlobalContext.Provider value={{
        projectState,
        projectDispatch,
        userState,
        userDispatch,
        tacheState,
        tacheDispatch,
        reunionState,
        reunionDispatch,
        messageState,
        messageDispatch,
        fileState,
        fileDispatch
        }}>
        {children}
    </GlobalContext.Provider>)
};

