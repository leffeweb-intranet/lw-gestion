

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router';
import { Reu } from '../../components/module/reunion/reunion';
import {ProjectDashboardUI} from '../../components/screen/ProjectDashboard/projectDashboardUI'
import { getProject } from '../../context/actions/project/getProject';
import { getReunions } from '../../context/actions/reunion/getReunions';
import { createReunion } from '../../context/actions/reunion/createReunion';
import { GlobalContext } from '../../context/Provider';
import Users from '../../components/screen/users';
import { listUserInformation } from '../../context/actions/user/listUserInformation';

export const ProjectsUsersContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client,
            users
        },
        userDispatch, 
        reunionState:{
          reunions,
          addReunion
        } ,
        reunionDispatch
    
    } =  useContext(GlobalContext);
     
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;

    /* REQUEST */

    useEffect(() => {
      listUserInformation()(userDispatch);
    }, [addReunion.loading]);


    useEffect(() => {
      getReunions(projectId)(reunionDispatch);
    }, [addReunion.loading]);

    const onSubmit=(values) => {
      console.log(values);

      values.date=values.date.format("YYYY-MM-DD");
      values.hours=values.hours.format("HH:mm");
      values.project=project?.data;
      
      createReunion(values)(reunionDispatch)
      console.log(values);

    }




    return <Users
    users={users}
    reunions={reunions}
    onSubmit={onSubmit}
        />
}
