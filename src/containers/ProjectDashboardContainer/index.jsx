

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router';
import {ProjectDashboardUI} from '../../components/screen/ProjectDashboard/projectDashboardUI'
import { getProject } from '../../context/actions/project/getProject';
import { getTaches } from '../../context/actions/tache/getTaches';
import { userInformation } from '../../context/actions/user/userInformation';
import { userPage } from '../../context/actions/user/userPage';
import { GlobalContext } from '../../context/Provider';
// import ProjectDashboard from '../../components/screen/ProjectDashboard'

export const ProjectDashboardContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:admin,
            userpage:client
        },
        userDispatch,  
    
    } =  useContext(GlobalContext);
    
    const {projectId,userId}=useParams();
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
    console.log("IIDI",client);
    /* REQUEST */

    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);

    useEffect(() => {
      userPage(userId)(userDispatch);
    }, []);



    return <ProjectDashboardUI
    userId={userId}
    client={client}
    admin={admin}
    projectId={projectId}
    collapse={{collaspeMenu,setcollaspeMenu,toggle}}
    project={project}  
  
    />
}
