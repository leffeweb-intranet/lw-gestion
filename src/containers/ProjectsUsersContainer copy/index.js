

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { getReunions } from '../../context/actions/reunion/getReunions';
import { createReunion } from '../../context/actions/reunion/createReunion';
import { GlobalContext } from '../../context/Provider';
import Users from '../../components/screen/users';
import { listUserInformation } from '../../context/actions/user/listUserInformation';

export const ProjectsUsersContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client,
            users
        },
        userDispatch, 
        reunionState:{
          reunions,
          addReunion
        } ,
        reunionDispatch
    
    } =  useContext(GlobalContext);
     
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;

    /* REQUEST */

    useEffect(() => {
      listUserInformation()(userDispatch);
    }, [addReunion.loading]);

    console.log('USERSSS',users);

    useEffect(() => {
      getReunions(projectId)(reunionDispatch);
    }, [addReunion.loading]);

    const onSubmit=(values) => {
      console.log(values);

      values.date=values.date.format("YYYY-MM-DD");
      values.hours=values.hours.format("HH:mm");
      values.project=project?.data;
      
      createReunion(values)(reunionDispatch)
      console.log(values);

    }


    console.log("reunions",reunions);
    console.log("add reu ",addReunion);



    return <Users
    users={users}
    reunions={reunions}
    onSubmit={onSubmit}
        />
}
