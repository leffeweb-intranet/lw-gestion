

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router';
import { Conversation } from '../../components/module/message/Conv';
import { OverviewUI } from '../../components/module/overview';
import { getProject } from '../../context/actions/project/getProject';
import { getUser, userInformation } from '../../context/actions/user/userInformation';
import { userPage } from '../../context/actions/user/userPage';

import { getMessages } from '../../context/actions/message/getMessages';
import { createMessage } from '../../context/actions/message/createMessage';

import { GlobalContext } from '../../context/Provider';

export const ProjectMessageContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:admin,
            userpage:client
        },
        userDispatch,
        messageState:{
          addMessage,
          messages
        },
        messageDispatch
    } =  useContext(GlobalContext);
     
     const {userId:idUser,projectId} =props.match.params;
    //  console.log(props.match.params);
    /* REQUEST */
    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);
    // console.log("idUser",idUser);

    useEffect(() => {
        userInformation(user.sub)(userDispatch);
      }, []);
      useEffect(() => {
        userPage(idUser)(userDispatch);
      }, []);
    useEffect(() => {
      getMessages(projectId)(messageDispatch);
    }, [addMessage.loading]);
    // console.log("client",client);

    const onSubmit=(values)=>{
        values.client=admin?.data;
      
        values.project=project?.data;

        console.log(values);
        createMessage(values)(messageDispatch);
    }
    console.log("messages",messages);

    const userId=admin.data?.id;
    return    <Conversation  
                client={client}
                form={{addMessage,onSubmit}}
                messages={messages}
                userId={userId}

                  />
}
