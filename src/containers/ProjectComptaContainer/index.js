

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { getProject } from '../../context/actions/project/getProject';
import { getFiles } from '../../context/actions/file/getFiles';
import {  userInformation } from '../../context/actions/user/userInformation';
import { GlobalContext } from '../../context/Provider';
import Livrable from '../../components/module/livrable/index';
import { createFile } from '../../context/actions/file/createFile';
import Compta from '../../components/module/livrable/index';

export const ProjectComptaContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
        fileState:{
          devis,
          addFile,
          factures
        },
        fileDispatch
    } =  useContext(GlobalContext);
     
     const {projectId} =props.match.params;
    /* REQUEST */
    useEffect(() => {
      if(projectId){
      getProject(projectId)(projectDispatch);}
    },[addFile.loading]);

    useEffect(() => {
        userInformation(user.sub)(userDispatch);
      }, []);
    
      console.log("projectId",projectId);
      useEffect(() => {
        if(projectId){
         getFiles(projectId,'devis')(fileDispatch);
         getFiles(projectId,'facture')(fileDispatch);

        }
      }, [addFile.loading]);
      
      console.log('factures',factures);
  
    const userId=user.data?.id;

    function buildFormData(formData, data, parentKey) {
      if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
        Object.keys(data).forEach(key => {
          buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
        });
      } else {
        const value = data == null ? '' : data;
    
        formData.append(parentKey, value);
      }
    }
    
    function jsonToFormData(data) {
      const formData = new FormData();
      
      buildFormData(formData, data);
      
      return formData;
    }

    const customRequest=({ onError, onSuccess, file })=>{
      console.log(file);
    let formJson={
      file:file,
      client:client.data,
      project:project.data,
      type:type
    }

    const form=jsonToFormData(formJson);

    createFile(form)(fileDispatch);

    }
    const onChange=()=>{
      getProject(projectId)(projectDispatch);
    }
    const onPreview = async file => {
      let src = file.url;
      if (!src) {
        src = await new Promise(resolve => {
          const reader = new FileReader();
          reader.readAsDataURL(file.originFileObj);
          reader.onload = () => resolve(reader.result);
        });
      }
      const image = new Image();
      image.src = src;
      const imgWindow = window.open(src);
      imgWindow.document.write(image.outerHTML);
    };
    const [type, settype] = useState("devis")
      const changeType=(value)=>{
        settype(value)
        console.log(type);
      }

      
    return    <Compta

                factures={factures}
                type={changeType}
                devis={devis}
                uploadAction={{onPreview,customRequest,onChange}}
                userId={userId}
                  />
}
