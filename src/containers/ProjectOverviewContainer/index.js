

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router';
import { OverviewUI } from '../../components/module/overview';
import {ProjectDashboardUI} from '../../components/screen/ProjectDashboard/projectDashboardUI'
import { getProject } from '../../context/actions/project/getProject';
import { getReunions } from '../../context/actions/reunion/getReunions';
import { getTaches } from '../../context/actions/tache/getTaches';
import { GlobalContext } from '../../context/Provider';
// import ProjectDashboard from '../../components/screen/ProjectDashboard'

export const ProjectOverviewContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
        tacheState:{
          taches
        },
        tacheDispatch,
        reunionState:{
          reunions
          } ,
        reunionDispatch,
    } =  useContext(GlobalContext);
     

    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;

     /* REQUEST */
    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);
    
    useEffect(() => {
      getTaches(projectId)(tacheDispatch);
    }, []);


    useEffect(() => {
      getReunions(projectId)(reunionDispatch);
    }, []);
    return    <OverviewUI  
                reunions={reunions}
                project={project}
                taches={taches}
                  />
}
