

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation, useParams, useRouteMatch } from 'react-router';
import { OverviewUI } from '../../components/module/overview';
import { Prestation } from '../../components/module/prestation/prestation';
import {ProjectDashboardUI} from '../../components/screen/ProjectDashboard/projectDashboardUI'
import { getProject } from '../../context/actions/project/getProject';
import { getTaches } from '../../context/actions/tache/getTaches';
import { GlobalContext } from '../../context/Provider';
// import ProjectDashboard from '../../components/screen/ProjectDashboard'

export const ProjectPrestationContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
        tacheState:{
          taches
        },
        tacheDispatch
    } =  useContext(GlobalContext);
     
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;
    console.log(props.match);
    /* REQUEST */
    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);
    
    console.log(project);
    useEffect(() => {
      getTaches(projectId)(tacheDispatch);
    }, []);
    return    <Prestation  
                project={project}
                taches={taches}
                  />
}
