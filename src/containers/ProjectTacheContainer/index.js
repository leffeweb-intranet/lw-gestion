

import { useAuth0 } from '@auth0/auth0-react';
import React, { useContext, useEffect, useState } from 'react'
import { Tache } from '../../components/module/Tache/tache';
import { getProject } from '../../context/actions/project/getProject';
import { createTache } from '../../context/actions/tache/createTache';
import { getTaches } from '../../context/actions/tache/getTaches';
import { GlobalContext } from '../../context/Provider';

export const ProjectTacheContainer = (props) => {
    const {
        user,
        getAccessTokenSilently,
        getAccessTokenWithPopup,
        isAuthenticated,
        logout
      } = useAuth0();

      const {
        projectState:{
            project
        },
        projectDispatch,
        userState:{
            user:client
        },
        userDispatch,
        tacheState:{
          addTache,
          taches
        },
        tacheDispatch
    } =  useContext(GlobalContext);
     
    const [collaspeMenu, setcollaspeMenu] = useState(false);
    const toggle=()=>{
      setcollaspeMenu(!collaspeMenu);
    }
     const {projectId} =props.match.params;

     const [visible, setvisible] = useState(false);
     const showDrawer = () => setvisible(true);
     const onClose = () => setvisible(false);
    
     const onSubmit=(values) => {
       console.log(values);
       values.project=project?.data;
       createTache(values)(tacheDispatch)
     }

    /* REQUEST */

    useEffect(() => {
      getProject(projectId)(projectDispatch);
    }, []);

    useEffect(() => {
      console.log("jy passe hein");
      getTaches(projectId)(tacheDispatch);
    }, [addTache.data]);

    const handleStatus=(elem,type)=>{
       let  editElem=elem;
        if (elem.status=="toDo"){  
          editElem.status="inProgress";
        }
        else if(elem.status=='inProgress'){
          if(type==-1){
            editElem.status="toDo"
          }else{
            editElem.status="done"
          }
        }
        else{
          editElem.status="inProgress";
        }
        onSubmit(editElem);
    };

    console.log("taches" ,taches);
    return <Tache  
            handleStatus={handleStatus}
            taches={taches}
            project={project}
            addProject={{visible,showDrawer,onClose,onSubmit,addTache}}
            />
}
