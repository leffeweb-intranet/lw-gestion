export default {
    tache: {
        loading: false,
        data: null,
        error: null
    },
    addTache: {
        loading: false,
        data: null,
        error: null
    },
    taches: {
        loading: false,
        data: {
            toDo: [],
            inProgress: [],
            done: []
        },
        error: null
    },
    editTache:{
        loading: false,
        data: null,
        error: null
    }
}