export default {
    project: {
        loading: false,
        data: null,
        error: null
    },
    addProject: {
        loading: false,
        data: null,
        error: null
    },
    projects: {
        loading: false,
        data: [],
        error: null
    },

}