export default {
    user: {
        loading: false,
        data: null,
        error: null
    },
    userpage: {
        loading: false,
        data: null,
        error: null
    },
    users: {
        loading: false,
        data: [],
        error: null
    },
    editUser: {
        loading: false,
        data: null,
        error: null
    },

    superUser: {
        loading: false,
        data: null,
        error: null
    },

}