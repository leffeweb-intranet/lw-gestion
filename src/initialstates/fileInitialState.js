export default {
    file: {
        loading: false,
        data: null,
        error: null
    },
    addFile: {
        loading: false,
        data: null,
        error: null
    },
    files: {
        loading: false,
        data: [],
        error: null
    },
    addDevis: {
        loading: false,
        data: null,
        error: null
    },
    devis: {
        loading: false,
        data: [],
        error: null
    },
    addFacture: {
        loading: false,
        data: null,
        error: null
    },
    factures: {
        loading: false,
        data: [],
        error: null
    },

}