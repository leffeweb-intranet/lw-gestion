import axios from "axios";
import { useAuth0 } from '@auth0/auth0-react';
import { getToken } from "../context/actions/auth/getScope";

export default (props)=>{
const baseUrl=process.env.REACT_APP_BACKEND_URL;

let header={};

// const {getAccessTokenSilently,getAccessTokenWithPopup}=useAuth0();
if(localStorage.token){
    header.Authorization=`Bearer ${localStorage.token}`;
}
console.log("props axios",props);

const axiosInstance = axios.create({
    baseURL: baseUrl,
    headers:header,
})

axiosInstance.interceptors.response.use(
    (response ) => 
    new Promise((resolve,reject)=>{
        resolve(response);
    }),
    (error)=>{
        // // console.log(error);
        if(!error.message){
            return new Promise((resolve,reject)=>{
                reject(error);
            });
        }
        if(error.response.status===403||error.response.status===401){

            localStorage.removeItem("token");
            // if(history){
            //     history.push("/auth/login")
            // }else{
            //     window.location = "/auth/login";

            // }
        }else{
            return new Promise((resolve,reject)=>{
                reject(error);
            });
        }

    }
    );
    return axiosInstance;

}
