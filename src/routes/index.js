import {
    DashboardOutlined ,
      UnorderedListOutlined,
      MessageOutlined,
      FilePdfOutlined,
      DeliveredProcedureOutlined,
      CalendarOutlined,
      EditOutlined,
      FolderOutlined} from '@ant-design/icons';
  import { ProjectDashboardContainer } from "../containers/ProjectDashboardContainer";
import HomeProjectContainer from "../containers/HomeProject";
import { ProjectOverviewContainer } from "../containers/ProjectOverviewContainer";
import { ProjectTacheContainer } from "../containers/ProjectTacheContainer";
import { ProjectPrestationContainer } from "../containers/ProjectPrestationContainer";
import { ProjectReunionContainer } from '../containers/ProjectReunionContainer';
import FirstFormContainer from '../containers/FirstFormContainer';
import { ProjectMessageContainer } from '../containers/ProjectMessageContainer';
import { ProjectComptaContainer } from '../containers/ProjectComptaContainer';
import { ProjectLivrableContainer } from '../containers/ProjectLivrableContainer';
import { ProjectFilesContainer } from '../containers/ProjectFilesContainer';
import { ProjectsUsersContainer } from '../containers/ProjectsUsersContainer';
   
export const projectRoutes=[
    {  
        path:'/project/:userId/:projectId/files',
        component:ProjectFilesContainer,
        icon:<FolderOutlined />,
        title:"Mes documents",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/livrable',
        component:ProjectLivrableContainer,
        icon:<DeliveredProcedureOutlined />,
        title:"Livrable",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/compta',
        component:ProjectComptaContainer,
        icon:<FilePdfOutlined />,
        title:"Devis",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/messages',
        component:ProjectMessageContainer,
        icon:<MessageOutlined />,
        title:"Messagerie",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/reunions',
        component:ProjectReunionContainer,
        icon:<CalendarOutlined />,
        title:"Réunion",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/prestation',
        component:ProjectPrestationContainer,
        icon:<EditOutlined />,
        title:"Prestation",
        needsAuth:false,
        isExact:true
        },
    {  
        path:'/project/:userId/:projectId/taches',
        component:ProjectTacheContainer,
        icon:<UnorderedListOutlined />,
        title:"Taches",
        needsAuth:false,
        isExact:true
        },
  {  
    path:'/project/:userId/:projectId',
    component:ProjectOverviewContainer,
    icon:<DashboardOutlined />,
    title:"Overview",
    needsAuth:false,
    isExact:true,
    home:true 
    },

] 
const routes = [

    // {
    //     path:'/user/project/:id',
    //     component:ProjectsUsersContainer,
    //     title:"Users",
    //     isExact:false,
    //     needsAuth:false
    // },
    
    {
        path:'/users',
        component:ProjectsUsersContainer,
        title:"Users",
        isExact:false,
        needsAuth:false
    },
    {
        path:'/project/:userId/:projectId',
        component:ProjectDashboardContainer,
        title:"Home",
        isExact:false,
        needsAuth:false
    },
    {
        path:'/premiere-connexion',
        component:FirstFormContainer,
        title:"Home",
        isExact:false,
        needsAuth:false
    },
    {
        path:'/user/projects/:userId',
        component:HomeProjectContainer,
        title:"Home",
        needsAuth:false,
        isExact:true,
        home:true
    },
    {
        path:'/',
        component:ProjectsUsersContainer,
        title:"Home",
        needsAuth:false,
        isExact:true,
        home:true
    }

]




export default routes;