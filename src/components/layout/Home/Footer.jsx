import { Col } from 'antd'
import { Footer } from 'antd/lib/layout/layout'
import React from 'react'

const FooterLW = () => {
    return (

    <Col className='headerLW' span={24}>

    <p style={{color:"white",textAlign:'center'}}> Copyright ©LeffeWeb 2021</p>
    </Col>
    )
}

export default FooterLW;
