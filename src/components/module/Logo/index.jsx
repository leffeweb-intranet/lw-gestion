import { NavLink, Redirect } from 'react-router-dom';
import './assets/styles.css';

export const Logo=({collapse,noPadding})=>{
    return(
        
        <a href='/'>
            <div className={noPadding ? "logo noPadding" :"logo"} >
                <h2 > {collapse ? 'LW' : 'LeffeWeb' }</h2>
            </div>
        </a>
    )
}