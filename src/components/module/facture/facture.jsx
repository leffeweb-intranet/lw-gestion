import { Card, Col, Row } from "antd";
import facture from './data.json';
import {FilePdfOutlined}  from "@ant-design/icons";
import './style.css';

export const Facture=()=>{
    return(
        <Row>
            <Col span={24}>
                <Card title={`Facture (${facture.files.length})`} bordered={false}>
                    <div className="site-card-wrapper">
                        <Row>
                            {facture.files.map(elem=><File file={elem}/>)}  
                        </Row>
                    </div>
                </Card>
            </Col>
        </Row>
    )
}

const File =({file})=>{
 return(
    <Col span={4} className={`facture ${file.paye ? "paye":"nonPaye"}`}>
      <Row  justify="center" style={{flexDirection:'column',textAlign:"center"}}>
        <FilePdfOutlined style={{margin:0}} label="dedez"/>
        <h3>{file.name}</h3>
        <span className="title">{file.date}</span>
        <span className="title">status: {file.paye  ?  "Payé" : "Non Payé" }</span>
      </Row>
    </Col>
 )


}