import { Card, Space ,DatePicker,Button, Radio,Form, TimePicker} from "antd";
import {useState} from 'react';
import {  Select} from 'antd';
import { Input } from 'antd';

const { TextArea } = Input;
const { RangePicker } = DatePicker;
const {Option}= Select;

export const AddReunion=({onSubmit})=>{
    let  handleChange=()=>{
         return ''
     }
     const [input, setInput] = useState(true);
       const [value, setValue] = useState();
 
     const onChange=(value, dateString)=> {
         console.log('Selected Time: ', value);
         // // // console.log('Formatted Selected Time: ', dateString);
       }
       const onOk =(value)=> {
         // // // console.log('onOk: ', value);
       }

     return(
                 <Card title="Demander une nouvelle réunion" bordered={false}>
                     <div className="site-card-wrapper">
                     <Space   direction="vertical" align="start">
                          <Form
                            layout="vertical"
                            onFinish={onSubmit}>
                              <Form.Item label={<h3>De quoi s'agit il ?</h3>} name="name">           
                                <Select defaultValue="newDevis" >
                                    <Option value="newDevis">Demande de prestations</Option>
                                    <Option value="issue">Problème rencontré</Option>
                                </Select>
                              </Form.Item>

                              <Form.Item label={<h3>Date</h3>} name="date">
                                <DatePicker  format='YYYY/MM/DD'  onChange={onChange} onOk={onOk} />
                              </Form.Item>

                              <Form.Item label={<h3>Heure</h3>} name="hours">
                                <TimePicker  
                                format='HH:mm'   
                                minuteStep={15} 
                                />
                              </Form.Item>

                              <Form.Item label={<h3>Durée</h3>} name="duration">
                                <Radio.Group>
                                    <Radio.Button value="15">15min</Radio.Button>
                                    <Radio.Button value="30">30min</Radio.Button>
                                    <Radio.Button value="60">1h</Radio.Button>
                                </Radio.Group>
                              </Form.Item>
                                                          
                              <Form.Item >
                                <Button type='primary' htmlType='submit' >Créer</Button>
                              </Form.Item>
                            </Form>           
                         </Space>
                             
                     </div>
                 </Card>
     )
 }