import { Card, Col, Row } from "antd";
import devis from './data.json';
import {FilePdfOutlined}  from "@ant-design/icons";
import './style.css';


export const Devis = () => {
    return(
        <Row>
            <Col span={24}>
                <Card title={`Devis (${devis.files.length})`} bordered={false}>
                    <div className="site-card-wrapper">
                        <Row>
                            {devis.files.map(elem=><File file={elem}/>)}
                            
                        </Row>
                    </div>
                </Card>
            </Col>
        </Row>
    )
}

const File = ({ file }) => {
 return(
    <Col span={4} className="devis" >
      <Row  justify="center" style={{flexDirection:'column',textAlign:"center"}}>
        <FilePdfOutlined style={{color:"#001529",margin:0}} label="dedez"/>
        <h3>{file.name}</h3>
        <span className="title">{file.date}</span>
      </Row>
    </Col>
 )


}