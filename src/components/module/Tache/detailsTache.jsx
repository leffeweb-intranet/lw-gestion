import { Button, Card, Divider, Tooltip,Space, Row, Col, Collapse, Progress, Badge, Input } from "antd"
import Form from "antd/lib/form/Form";
import TextArea from "antd/lib/input/TextArea";
import data from './commentaire.json';
import {SendOutlined,SmileOutlined,PaperClipOutlined,CheckCircleOutlined,CloseCircleOutlined } from '@ant-design/icons';
import Checkbox from "antd/lib/checkbox/Checkbox";
import  listTache from  "./data.json";
import React, { useState } from 'react';
const { Panel } = Collapse;


const text="Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemue velit nisi, pretium ut lacinia in, elementum id enim. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem."

export const DetailsTache=(props)=>{
const [formAddSStache, setFormAddSStache] = useState(false);

const handleAddSStache=()=>{
    setFormAddSStache(true)
}
const tache=listTache.tache[0];
const handleFormAddTache=()=>{
    setFormAddSStache(!formAddSStache)
}
    return(
        <>
        <Space>
            <Card title={<Top title={tache.title}  handleFormAddTache={handleAddSStache} status='in progress' />}>
                
                <p>Aenean sed vehicula massa, eget venenatis sapien. Nam vel elit enim. Morbi in arcu enim. Donec scelerisque auctor libero, eget vulputate ante. Nam vestibulum lorem non imperdiet posuere. Duis ex sem, faucibus a imperdiet non, molestie viverra nisi. Donec id tempus nulla. Vestibulum pretium nisl vel ultricies interdum. Sed hendrerit, tortor sed egestas rutrum, sem nulla faucibus tortor, sit amet vestibulum risus mauris a ligula.</p>
                <Divider/>
                <SousTache listSousTache={tache.sousTache} handleFormAddTache={handleFormAddTache} formAddTache={formAddSStache}/>
            </Card>
        

            </Space>
            <EspaceCommentaire/>
        </>

    )
}


const EspaceCommentaire=()=>{

    return(
        <>
        <Divider />
        <h3>Commentaires</h3>
   
        {data.commentaires.map(
        commentaire=> <Card title={commentaire.author} bordered={false}> {commentaire.message} </Card>
        )}
   
        <Form>
            <Row>
                <Col span={20}><TextArea /></Col>
               
                   <Col span={2} >
                   <Row  justify='space-around'>
   
                   <Tooltip title="Ajouter une icône">
   
                   <Button type="primary" shape="circle" icon={<SmileOutlined />} />
                   </Tooltip>
   
   
                   <Tooltip title="Joindre un document">
   
                   <Button type="primary" shape="circle" icon={    <PaperClipOutlined />} />
                   </Tooltip>
   
                   <Tooltip title="Envoyé">
   
                       <Button type="primary" shape="circle" icon={<SendOutlined />} />
                   </Tooltip>
   
                   </Row> 
                   </Col>
               </Row>
   
               </Form>
           </>
    )
}


const Top = ({title,status,handleFormAddTache})=>{

    return(
        <div style={{display:'flex',width:'100%',justifyContent:'space-between'}} ><Space><h2> {title} </h2><p  className='status inProgress'>{status}</p></Space><Button onClick={handleFormAddTache}> Ajouter une sous-tâche</Button></div>
    )
}



const SousTache =( {listSousTache,formAddTache,handleFormAddTache})=>{
 
    const progessSST=(listSousTache.filter(tache=> tache.isFinish==true).length / listSousTache.length)*100;
    // // // console.log(progessSST); 
       return(
       <>
        <h3> Sous tache </h3>
        <Progress percent={progessSST} strokeColor={{'0%': '#108ee9','100%': '#87d068',}} />
            <Collapse  accordion>
            {listSousTache.map((tache,i)=>{
                    return (
                    <Panel header={<Checkbox checked={tache.isFinish ? true : false}  >{tache.title}</Checkbox> } key={i}>
                        <p>{tache.description}</p>
                    </Panel>

                )}
            )}
            {formAddTache ?
             <Panel header={<div style={{display:'flex',justifyContent:'space-between',width:'100%'}}><Input style={{marginRight:'1em'}}  placeholder='My new sstache' /><Space><Tooltip  title='Confirmer'> <Button style={{background:'#F8B195',color:'white'}}><CheckCircleOutlined /></Button></Tooltip> <Tooltip title='Annuler'><Button style={{background:'#F67280',color:'white'}} onClick={handleFormAddTache}><CloseCircleOutlined /></Button></Tooltip> </Space> </div>} >
                        <TextArea />
                    </Panel>
                    :
                    ""
            }

            </Collapse>
        </>
    )
}