
import { Table, Button, Row, Col, Card, Badge,Progress, Space, Image, Tag } from 'antd';
import { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './assets/style.css';
import Icon, {CaretLeftOutlined, CaretRightFilled, PlusSquareOutlined,StepBackwardOutlined,StepForwardOutlined,TableOutlined} from '@ant-design/icons';
import TextTruncate from 'react-text-truncate';
import kabanIcon from './assets/kabanIcon.svg'
import { AddTache } from './addTache';
const { Meta } = Card;

export const Tache=({addProject,taches,handleStatus})=>{
  const [ kabanView,setkabanView] =useState(false)
  const handleView=()=>{
    setkabanView(!kabanView);
  } 

  return(

        <div className="site-card-wrapper">
        <Row gutter={[16,16]} >
            <Col lg={15} md={24} >     
            <Button onClick={handleView} > {kabanView ? <Image preview={false} width={30} src={kabanIcon}/> : <TableOutlined  sizes="large" /> } </Button>

               { kabanView ?
               <Card style={{marginTop:'1em'}} title="Tâche" bordered={false}>
                  <TableTache {... taches.data}/>
                </Card>
                :
                <Kaban handleStatus={handleStatus}  {... taches.data} />}
            </Col>
            <Col lg={6} md={24}>  
             <Informations  {... taches.data}  />
            <div style={{marginTop:'1em'}}>
            <AddTache {... addProject}/>
            </div>
            </Col>
        </Row>
        

    </div>        
    )
}

const Informations=({toDo,inProgress,done})=>{
  const data=toDo.concat(inProgress).concat(done);
  console.log(data);

  console.log(data.length);
  return(
       
    <Card title="Informations" bordered={false}>
    <h3> À faire ({toDo.length}) </h3>
    <Progress percent={Math.round((toDo.length/data.length)*100)} size="small" strokeColor="#1890ff" status="active" />
  
  <h3> En cours ({inProgress.length}) </h3>
  <Progress percent={Math.round((inProgress.length/data.length)*100)} size="small" strokeColor="#faad14"  status="active" />
  
  <h3>Tâche finis({done.length}) </h3>
  <Progress percent={Math.round((done.length/data.length)*100)} size="small" strokeColor={{'0%': '#108ee9','100%': '#87d068',}} status="active" />

  </Card>

  );
}

const columns = [
  {
    title: 'Titre',
    dataIndex: 'name',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    render: status =>{
      let  text;
      let  color;

      switch  (status){
        case 'inProgress':
          text="En cours"
          color="rgb(250, 173, 20)";

          break;
      case 'toDo':
          text="À faire "
          color="rgb(24, 144, 255)";

          break;
      default:
        text="Terminé";
        color="#389e0d";
        break
      }
      
      return <Tag 
    color={color}>
      {text}

    </Tag>
    },

  },
  {
    title: 'Description',
    dataIndex: 'description',

  },
];

const TableTache=({toDo,inProgress,done})=> {
  const data=toDo.concat(inProgress).concat(done);

 const setStatusSort = () => {
    setSortedInfo({
              order: 'descend',
        columnKey: 'age',
      });
  };

  const [  filteredInfo,setFilteredInfo]= useState(null);
  const [sortedInfo,setSortedInfo]= useState(null);

  const [selectedRowKeys,setSelectedRowKeys]= useState([]);
  const [loading,setLoading]= useState(false);

  const  start = () => {
    setLoading(true);

    setTimeout(() => {
      
        setSelectedRowKeys([]);
        setLoading(true);
    }, 1000);
  };

  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys({ selectedRowKeys });
  };
    const rowSelection = {
      selectedRowKeys,
      onChange: onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        <div style={{ marginBottom: 16 }}>

          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Table  columns={columns} dataSource={data}  /> 
      </div>
    );
  
}


const Kaban = ({toDo,inProgress,done,handleStatus})=>{

  console.log("toDo",toDo);
  return(
    <Row style={{marginTop:'1em'}}>
      <Space align='start' size={20}>
      <Col >
          <Card className='kabanCol do' title={`À faire (${toDo.length})`}>
          <Space direction='vertical' >
           {toDo.map(elem=>  <KabanItem  handleStatus={handleStatus} elem={elem}/>)}
           </Space >
          </Card>
          </Col>
        <Col >
            <Card className='kabanCol progress' title={`En cours (${inProgress.length})`}>
            <Space direction='vertical'>
              {inProgress.map(elem=> <KabanItem handleStatus={handleStatus} elem={elem}/> )}
              </Space >
           </Card>
           </Col>

           <Col >

          <Card className='kabanCol don' title={`Terminé (${done.length})`}>
            <Space direction='vertical'>
              {done.map(elem=> <KabanItem  handleStatus={handleStatus} elem={elem}/>)}
            </Space >
          </Card>
          </Col>

      </Space>

    </Row>
  )

}


const  KabanItem=({elem,handleStatus})=>{

  const action=[];

  switch(elem.status){
    case "toDo":
      action.push(<StepForwardOutlined   onClick={()=>handleStatus(elem,1)}  />);
      break;

    case "done":
      action.push(<StepBackwardOutlined   onClick={()=>handleStatus(elem,-1)}  />);
      break;
    default:
      action.push(<StepBackwardOutlined   onClick={()=>handleStatus(elem,-1)}  />)
      action.push(<StepForwardOutlined   onClick={()=>handleStatus(elem,1)}  />)
      break;

}

  return <Card className='kabanItem' className='kabanItem'  title={elem.name}
  actions={action}
  > 
  <TextTruncate line={3} text={elem.description}/>

 
   </Card>
   
}