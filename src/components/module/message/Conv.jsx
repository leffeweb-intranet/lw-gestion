import { Button, Card, Divider, Space, Tooltip,Form } from "antd";
import './style.css';
import {useEffect, useRef, useState} from 'react';
import {  Select} from 'antd';
import { Input } from 'antd';

import { PaperClipOutlined, SendOutlined, SmileOutlined } from "@ant-design/icons";
import moment from "moment";
const { TextArea } = Input;

const {Option}= Select;
export const Conversation=({messages,form,userId,client})=>{
  const messageEl = useRef(null);
  const [zonearea, setzonearea] = useState("de");
  useEffect(() => {
    if (messageEl) {
      messageEl.current.addEventListener('DOMNodeInserted', event => {
        const { currentTarget: target } = event;
        target.scroll({ top: target.scrollHeight, behavior: 'smooth' });
      });
    }
  }, [])

  const zone=useRef();
   let  handleChange=()=>{
        return ''
    }
    const handle=(values)=>{
      form.onSubmit(values);
      console.log("zonearea",zonearea);

      setzonearea('');
      console.log("zonearea",zonearea);

    }
    
    const [input, setInput] = useState(true);
    return(
        <>
        <Card  style={{height:"100%",justifyContent:"space-evenly"}} className="messages" title="message"
          span={20}>
         <div className='contentBox' id='scroller' ref={messageEl} >
             {messages.data.map(elem=><Message client={client} userId={userId} message={elem}/>)}

        </div>
        <Form
        onFinish={handle}
        >
        <div className='footer' >
          <Form.Item
          style={{width:"100%"}}
          name="content"
          >
          <TextArea  value={zonearea} onChange={(text)=>setzonearea(text)}  />
          </Form.Item>
        <Space>

        <Tooltip title="Ajouter une icône">

        <Button type="primary" shape="circle" icon={<SmileOutlined />} />
        </Tooltip>

        
        <Tooltip title="Joindre un document">

        <Button type="primary" shape="circle" icon={<PaperClipOutlined />} />
        </Tooltip>

        <Tooltip title="Envoyé">

            <Button type="primary" loading={form.addMessage.loading} htmlType="submit" shape="circle" icon={<SendOutlined />} />
        </Tooltip>

        </Space>
          </div>
          </Form>
           </Card>
        </>
    );

}


const Message=({message,userId,client})=>{
const date =moment(message.createdAt).format('MM/DD/YYYY à HH:mm');
const me=userId==message.client?.id;
console.log('message',message);
// console.log(client);
return(
<>

      <div className={me ? 'boxMessage send':'boxMessage receive'} >
        <p>{me ? 
        message.client?.firstName
        :
       `${client?.data?.firstName}  ${client?.data?.lastName}`
        }  
         <b>
          {/* {message.author } */}
        </b>  <span>Le {date} </span> </p>
        <Divider />
          <p>{message.content} </p>
      </div>
      </>
)
}
