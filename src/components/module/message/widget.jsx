import { Card, Table } from "antd"
import  data from './conversation.json'

const columns = [
    {
      title: 'ticket',
      dataIndex: 'title',
    }];

export const MessageWidget=()=>{
    return(
        <Card title='Mes dernier ticket'>
                <Table showHeader={false} columns={columns} dataSource={data.conversation} />

        </Card>
    )

}