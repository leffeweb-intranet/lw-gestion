import { Form, Input, Button, Card, Col, Spin, Image } from 'antd';
import axios from 'axios'

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const getProject= async (getAccessTokenSilently,userSub,projectID)=>{

  try {
      const token = await getAccessTokenSilently({
          audience: 'http://localhost:5000/api',
          scope: 'read:project',
      });

      const result = await fetch(`http://localhost:5000/api/client/${userSub}`, {
          method: 'GET',
        headers: {
          authorization: `Bearer ${token}`
        }
      });
      const data = await result.json();

      const config = {
          headers: { Authorization: `Bearer ${token}` }
      };
      const project = await axios.post(`http://localhost:5000/api/project/`,{ id:projectID},config);
      return project.data;

  } catch (error) {
  }

}
/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

/* eslint-enable no-template-curly-in-string */
export const Prestation = ({project}) => {

  const onFinish = (values) => {
  };
  if(project.loading) return <Spin/>
  return (
    <Col  lg={10} sm={22}>

    <Card title="Préstation">
            <Form {...layout}
             name="nest-messages" 
             onFinish={onFinish} 
             validateMessages={validateMessages} 
             initialValues={project.data}
             labelAlign='left' >
            <Image
            src={project.data?.image}
            />
            <Form.Item name={'image'}  label="Name of project" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>

                <Form.Item name={'name'}  label="Name of project" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={'url'} label="Lien du site web">
                    <Input  />
                </Form.Item>
                <Form.Item name={'description'} label="Description">
                    <Input.TextArea />
                </Form.Item>
                <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                    Sauvegarder
                    </Button>
                </Form.Item>
           
                </Form>
                
      </Card>
    </Col>
  );
  }