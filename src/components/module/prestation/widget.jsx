import { Card, Image, Skeleton } from "antd"

export const PrestationWidget = ({project})=>{
    return(
        <Card  title={<Skeleton loading={project?.loading}>{project.data?.name}</Skeleton>} bordered={false}>
            <Image     src={project?.data?.image}/>
            <Skeleton 
            loading={project?.loading}
            >
                {project.data?.description}
            </Skeleton>
        </Card>

    )   
}