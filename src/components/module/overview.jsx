import { Card, Col, Divider, Image, Progress, Row, Space } from "antd"
import Grid from "antd/lib/card/Grid"
import { useState } from "react"
import { MessageWidget } from "./message/widget";
import  {ReuWidget}  from "./reunion/widget";
import { TacheWidget } from "./Tache/widget";
import { PrestationWidget } from "./prestation/widget";

const productsData= [{label:'Design Logo',price:45},
            {label:'Debugg Wordpress',price:150},
            {label:'Création de module',price:400}]

export const OverviewUI=({project,taches,reunions})=>{
    // console.log('le project',project);
    return(
        <div className="site-card-wrapper">
            <Row type="flex"  gutter={[16,10]}  >
                <Col flex='2' >     
                <Space direction='vertical' >

                 <PrestationWidget project={project}/>
                    </Space>
                </Col>

                <Col flex='2' >     
                <Space direction='vertical' >
                    <Price products={productsData}/>
                    <ReuWidget reunions={reunions}/>
                </Space>
                </Col>
            
                <Col flex='1' >     
                <Space direction='vertical'>
                  <MessageWidget/>
                    <TacheWidget  {...taches}/>
                </Space>

                    </Col>
                    
            </Row>

        </div>
    )
}


const LinePrice=({product})=>{
    return(
            <>    
             <Col span={12}>
              <p>{product?.label}</p>
              </Col>
             <Col span={12}> 
                    <span className="Price">{product?.price} €</span>
             </Col>
             <Divider dashed/>
             </>
            
        );
}

const Price=(props)=>{
    const productList = props.products;
    const ess=productList.map(element => {
        return element.price;
    });
    const t = ess.reduce((a,b)=>a+b,0);
  //  productList.reduce();
    return(
    <Card title="Couts" bordered={false}>
    
    <Row className="priceBox">
    {productList.map((element)=>(<LinePrice product={element}/>))  }  
    <Col span={12}> <p style={{fontWeight:700}}>Total</p></Col>

    <Col span={12}> <span  style={{fontWeight:700}} className="Price">{t} €</span> </Col>
    </Row>
        
    </Card>
        );
}

