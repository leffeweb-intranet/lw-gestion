import React from 'react';

import { Card,Image,Row,Upload } from "antd"
import './style.css'
import { FilePdfOutlined } from '@ant-design/icons';
export const Mydocs =({upload})=>{



    return(
        <Card title="Mes documents partagé">
            <Demo upload={upload}/>
        </Card>
        )
}

const Demo = ({upload}) => {
    return (
      <Row>
      {upload.files.data.map(
        file=> <ItemRender file={file}/>
      )}
        <Upload
          style={{display:'flex',width:"auto"}}
          onChange={upload.onChange}
          customRequest={upload.customRequest}
          listType="picture-card"
          className="avatar-uploader"

          showUploadList={false}
          >
          {'+ Upload'}
        </Upload>
        </Row>
    );
  };
  


const truncateString=(str, num) =>{
  if (str.length <= num) {
    return str
  }
  return str.slice(0, num) + '...'
}

export const ItemRender=({file})=>{

  console.log(file);
  var ext = file.path.substr(file.path.lastIndexOf('.') + 1);
  console.log(ext);
  const pdf=ext=="pdf";
  const url=`http://localhost:5000/api/file/up/${file.path}`;
    
  const Imgf=()=>{
    if(pdf){
      return <a  target="_blank" href={url} ><FilePdfOutlined style={{ fontSize: '50px',fill:'rgb(0,21,41)!important'  }}/></a>
    }
    return(
      <Image
      src={url}
      style={{maxWidth:"200px"}}
      />
    )
  }
   return(
     <Card title={truncateString(file.name,10)} className="itemFile">
      <div>
        <Imgf/>
      </div>
     </Card>
   )
  }