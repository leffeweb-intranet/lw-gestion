
import { Card, Col, Row, Image } from "antd";
import data from './data.json';
import sketch from './img/sketch.png'
import adobeXD from './img/adobeXD.png'
import "./style.css";

export const Design=()=>{
    return(
        <Row>
            <Col span={24}>
            <Card title="Design" bordered={false}>
                <div id="design">
                    <Row  justify="space-around">
                        {data.files.map(elem=> <Des file={elem}/>)}
                    </Row>
                </div>
            </Card>
            </Col>
        </Row>
    )
}

const Des=({file})=>{
    return(
        <Col  className="designBox"  span={4}>
            <Card   title={file.name}>
                <div>
                    <Image width={100} src={file.adobe ? adobeXD : sketch} />
                </div>
            </Card>
        </Col>
    );
}