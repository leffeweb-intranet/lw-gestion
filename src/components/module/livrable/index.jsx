import { Image, Row, Tabs, Upload } from 'antd'
import React from 'react'
import { AppleOutlined, AndroidOutlined, FilePdfOutlined } from '@ant-design/icons';
import { ItemRender } from '../mydocs/mydocs';
import { Facture } from '../facture/facture';

const { TabPane } = Tabs;

const Compta = ({factures,type,devis,uploadAction}) => {
    return (

      <Tabs 
        onChange={type}
     defaultActiveKey="devis">
        <TabPane
          tab={
            <span>
              <AppleOutlined />
              Devis
            </span>
          }
          key="devis"
        >
          <Demo data={devis.data} uploadAction={uploadAction}/>
        </TabPane>
        <TabPane
          tab={
            <span>
              <AndroidOutlined />
              Facture
            </span>
          }
          
          key="facture" >
          <Demo  data={factures.data} uploadAction={uploadAction}/>

        </TabPane>
      </Tabs>
    )
}

const Demo = ({data,uploadAction}) => {
  console.log("upload",uploadAction);
  console.log("data",data);

  return (
    <Row>
    {data?.map(
      file=> <ItemRender file={file}/>
    )}
      <Upload
        style={{display:'flex',width:"auto"}}
        onChange={uploadAction.onChange}
        customRequest={uploadAction.customRequest}
        listType="picture-card"
        className="avatar-uploader"

        showUploadList={false}
        >
        {'+ Upload'}
      </Upload>
      </Row>
  );
}
export default Compta
