import React from 'react';
import './assets/style.css';
import { Row, Col, Button, Space, Divider, Empty, Spin } from 'antd';
import { AppstoreAddOutlined, TagsOutlined  } from '@ant-design/icons'; 
import Search from 'antd/lib/input/Search';
import { Project } from './project-item';




const Projects=({client,data})=>{    
    // // console.log('data',data);      
    if(data.length  ==0) return <Empty/>
    return  data.map((element) => <Project client={client} data={element} />)

}

export default function GridProject({client,projects,showModal}){
    console.log("mon client",client);
        return(
        <Col  style={{margin:'auto'}} span={20} > 
                <Row justify='space-between' style={{marginTop:'1em'}}>  
                <Col span={3} push={1} className="find" > 
                <p style={{margin:'auto'}}>Client: {client?.data?.firstName} {client?.data?.lastName} </p>
                </Col>
                    <Col span={5} push={1} > 
                        <Search
                        placeholder="Rechercher un projet.."
                        enterButton="Search"
                        size="large"
                        suffix={<TagsOutlined  />}
                        onSearch="{onSearch}"
                        />   
                    </Col>
                    <Col className="find" > 
                    <Space  direction="horizontal" align="center">
                        <p style={{margin:'auto'}}> {projects?.data?.length} projets ouvert dont  {projects?.data?.length}  finis.</p>
                        <Button type="primary"  icon={<AppstoreAddOutlined />} onClick={()=>showModal()}>
                            Ajouter un projet
                        </Button>
                    </Space>
                    </Col>
                    <Divider/>
                </Row>
            <Row gutter={[20,20]} >
                {projects.loading ?
                        <Spin/>
                        :
                        <Projects client={client} data={projects?.data}/> 
                    }
            </Row>
           </Col>
    );
}