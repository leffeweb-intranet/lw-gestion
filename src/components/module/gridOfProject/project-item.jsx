import { Row, Col,Skeleton, Card} from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined  } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import TextTruncate from 'react-text-truncate';
import React from 'react';
const { Meta } = Card;

export  function Project({data,client}){
    const name= data.name;
    const description= data.description;
    const image= data.image;
    const clientId=client?.data?.id;
    const url =`/project/${clientId}/${data.id}`;
    const configure ="/project/"+data.id+'/prestation';
    console.log(url);
 return(
     <Col>
        <Card
        style={{ width: 500}}
        actions={[
            <a href={url} >
            <SettingOutlined key="setting" />
            </a>,
            <a href={configure} >
            <EditOutlined key="edit" />
            </a>,
            <EllipsisOutlined key="ellipsis" />,
        ]}
        >
            <Skeleton loading={data.loading} avatar active>
                <Row justify="space-around">
        
                <Col span={10}>
                    <div style={{backgroundSize:'cover',backgroundPosition:"center",backgroundImage:`url(${image})`,width:190,height:190}} />
                </Col>
                <Col span={12 } >
                    <Meta
                    title={name}
                    description={<TextTruncate
                        line={7}  
                        text={description}
                        />} 
                    />
                </Col>
                </Row>
            </Skeleton>
        </Card>
     </Col>
 );
 }