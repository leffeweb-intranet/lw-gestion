import React from 'react';
import { Row, Col,Dropdown, Menu} from 'antd';
import GridProject from '../../module/gridOfProject';
import {Logo} from '../../module/Logo';
import './assets/style.css';
import { DownOutlined,UserOutlined} from '@ant-design/icons';
import Modal from 'antd/lib/modal/Modal';
import { AddProject } from './addProject';
import FooterLW from '../../layout/Home/Footer';

const Home = ({logout,modal,projects,formAddProject,client,user}) => {
    return(
        <>
    <div   style={{ minHeight: '96vh'}} >
        <Row>
            <Col className='headerLW' span={24}>
                <Row justify="space-between">
                <Col span={2} >
                    <Logo noPadding/>
                </Col>
                <Col span={3}>
                    <MenuRight client={user} logout={logout} client={{}}/>
                    
                </Col>
                </Row>
            </Col>
            
            </Row>
        <Col span={24}> 
            <GridProject  client={client} showModal={modal.showModal}  projects={projects} />
            
            </Col>
            <Row>

            </Row>
    </div>
         <Row style={{
            height:'1vh',
            width:'100%',
      bottom:0,
       }}>
           <FooterLW/>
            </Row>
            <Modal title="Ajouter  un projet" visible={modal.modal} onOk={modal.closeModal} onCancel={modal.closeModal}>
                <AddProject {... formAddProject} {...  user}  />
            </Modal>
            </>
    );

}



const MenuRight =  ({match,logout,client})=>{
    console.log("client",client);
    return(
        <Dropdown overlay={<Drop logout={logout}/>} >
    
        <a className="topMenu" onClick={e => e.preventDefault()}>
    
        <UserOutlined />
         {client.data?.firstName } {client.data?.lastName } <DownOutlined />
        </a>
      </Dropdown>
    )
}

  
    const Drop= ({logout})=>{
    return(   
        <Menu>
        <Menu.Item>
        <a rel="noopener noreferrer" href="#">
            Mes informations
        </a>
        </Menu.Item>
    
    
        <Menu.Item>
        <a rel="noopener noreferrer" href="#">
        Vue Globale
        </a>
        </Menu.Item>
        <Menu.Item danger onClick={logout}>Déconnexion</Menu.Item>
        </Menu>
    )
    }
export default Home
