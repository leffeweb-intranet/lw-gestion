import React, { useState } from 'react';
import {
  Form,
  Input,
  Button,
  Switch,
  Upload,
  Spin
} from 'antd';

import { UploadOutlined } from '@ant-design/icons';

import TextArea from 'antd/lib/input/TextArea';


export const AddProject = ({loading,data,onSubmit}) => {

  const [image, setImage] = useState();
  const [isUrl, setIsUrl] = useState(false);

  const handleIsUrl=()=>{
    setIsUrl(!isUrl);
  }
  const e=async (value)=>{
  setImage(value.file);
  }

  if(loading) return <Spin/>

  return (
    <Form
        labelCol={{ span: 5 }}
        wrapperCol={{ span:17 }}
        layout="horizontal"
        onFinish={onSubmit}
      >

        <Form.Item label="Nom du Projet" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="Lien vers le site" name="url">
          <Input />
        </Form.Item>

        <Form.Item label="Description" name="description">
          <TextArea />
        </Form.Item>
        
        <p>url image</p>
        <Switch  onChange={handleIsUrl} />

        { isUrl  ?
        <>
                <Form.Item label="Image" name="image">
               <Input />
              </Form.Item>
              
              </>
        :

          <Upload onPreview={true} onChange={e} >
    <Button icon={<UploadOutlined />}>Click to Upload</Button>
        </Upload>
          
        }
        
        <Form.Item >
          <Button type='primary' htmlType='submit' >Créer</Button>
        </Form.Item>
      </Form>
    );
};







