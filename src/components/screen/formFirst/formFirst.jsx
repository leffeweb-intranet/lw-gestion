import { Form, Input, Button, Layout,Col, Card, Alert, Space } from 'antd';
import * as axios from 'axios';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
 export const FormFirst = ({onSubmit,user}) => {
//   const onFinish = async (values) => {
//       values.idAuth=user.sub;
    
//     try {
//         const token = await getAccessTokenWithPopup({
//             audience: 'http://localhost:5000/api',
//             scope: 'read:project',
//         });
//         const config = {
//           headers: { Authorization: `Bearer ${token}` }
//         };
//         const result = await axios.post(`http://localhost:5000/api/client/`, values,config);
//         const data = await result.json();
//         handlerInfo(true);
        
//     } catch (error) {
//         // // // console.log(error);
//     }
//   };

  return (
    <Layout style={{height:'100vh'}}>
            <Col span={15} style={{margin:' auto auto'}} >
              <Card title='  Bienvenue !'>
                  <Alert
                   type="info"
                  message=' Pour créer votre  espace, nous avons besoin  de quelque informations'/>  
                  <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                      remember: true,
                    }}
                    layout="vertical"
                    onFinish={onSubmit}
                  >
                                    <Space direction="vertical" style={{width:"100%"}}>
                    <Form.Item
                      label="Votre nom"
                      name="firstName"
                      initialValue={user?.family_name}
                      rules={[
                        {
                          required: true,
                          message: "Merci de renseigner votre prénom",
                        },
                      ]}
                    >    
                            <Input />
                            </Form.Item>
                      <Form.Item
                    label="Votre Prénom"
                    name="lastName"
                    initialValue={user?.given_name}

                    rules={[
                      {
                        required: true,
                        message: "Merci de renseigner votre nom",
                      },
                    ]}
                  >
                      <Input />
                    </Form.Item>
                    <Form.Item
                    label="Votre Numéro de téléphone"
                    name="Tel"
                  >
                      <Input />
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                      <Button type="primary" htmlType="submit">
                        Submit
                      </Button>
                    </Form.Item>
                    </Space>
                  </Form>
                  </Card>
                  </Col>
                  </Layout>
  );
};
