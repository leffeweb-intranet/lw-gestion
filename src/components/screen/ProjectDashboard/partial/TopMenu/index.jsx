import { DownOutlined,UserOutlined,QuestionCircleOutlined, UserSwitchOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu, Space } from "antd";
import './assets/style.css'
import { useAuth0 } from '@auth0/auth0-react';
import { NavLink } from "react-router-dom";


 const TopMenu=({client,showDrawer,user})=>{
  const {
    isLoading,
    isAuthenticated,
    error,
    loginWithRedirect,
    logout,
  } = useAuth0();
  console.log("user",user);  
return(
<Space>

  {user?.data?.admin
  &&
     <a className="topMenu" href="/users"> <UserSwitchOutlined/> Clients</a>
  }
    <Dropdown overlay={< MenuDeroul logout={logout}/>} style={{display:"flex",justifyContent:"center",flexDirection:"column"}} >

    <a className="topMenu" style={{display:"flex",justifyContent:"center",flexDirection:"row"}} onClick={e => e.preventDefault()}>

    <UserOutlined style={{display:"flex",justifyContent:"center",flexDirection:"column"}}/>
        
       {user?.data?.firstName}  {user?.data?.lastName} <DownOutlined style={{display:"flex",justifyContent:"center",flexDirection:"column"}} />
    </a>
  </Dropdown>
  
  <a  className="topMenu" style={{display:"flex",justifyContent:"center",flexDirection:"row"}} onClick={()=>showDrawer()}>
  <QuestionCircleOutlined style={{display:"flex",justifyContent:"center",flexDirection:"column"}} />
  <p  style={{display:"flex",margin:"auto",marginLeft:'0.1em'}}>Help</p>

</a>

  </Space>
);
}


const MenuDeroul= ({logout})=>{
  
  return(
    <Menu>
    <Menu.Item>
    <a rel="noopener noreferrer" href="#">
        Mes informations
    </a>
    </Menu.Item>


    <Menu.Item>
    <a rel="noopener noreferrer" href="#">
    Vue Globale
    </a>
    </Menu.Item>
    <Menu.Item danger onClick={logout} >Déconnexion </Menu.Item>
    </Menu>

);}

export default TopMenu;