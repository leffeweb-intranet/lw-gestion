import { Menu, Button, Dropdown } from 'antd';
import React,{useState} from 'react';
import {
  DashboardOutlined ,
    UnorderedListOutlined,
    UploadOutlined,
    MessageOutlined,
    FilePdfOutlined,
    DeliveredProcedureOutlined,
    CalendarOutlined

} from '@ant-design/icons';
import { 
  BrowserRouter as Router, 
  Route, 
  Link, 
  Switch, 
  NavLink
} from 'react-router-dom'; 
import './assets/style.css';
import {Logo} from '../../../../module/Logo';
import { projectRoutes } from '../../../../../routes';
const { SubMenu } = Menu;

export const LeftMenu =({collapse,projectId,userId})=>{
    
    return (
    <>
    
        <Logo collapse={collapse} />
        <Menu 
        defaultSelectedKeys={["Overview"]}   
        // onSelect={(element)=>props.setSelection(element.key)}
         theme="dark" 
        mode={collapse.collapseMenu ? "vertical-right":"inline"} 
        className={collapse.collapseMenu && "mini"} >

        {  projectRoutes.slice(0).reverse().map((route,index)=>{      
            let pathname=route.path.replace(':projectId',projectId);
            // const pathname=pathname1.path.replace(':userId',userId);
             pathname=pathname.replace(':userId',userId);            
            return(
              <Menu.Item key={index} icon={route.icon}>
              <NavLink to={pathname}>{route.title}</NavLink>
            </Menu.Item>
            )
      }
        )
        }
       
        </Menu>
        


    </>
    );
  }

  
  const LivrableColapse=()=> (
      <Dropdown overlay={subLivrable} >
  
      <a  onClick={e => e.preventDefault()}>
    
       <DeliveredProcedureOutlined />
      </a>
    </Dropdown>

)

const subLivrable=(<Menu>
    <Menu.Item key="Informations">
    <a rel="noopener noreferrer" href="#">
        Mes informations
    </a>
    </Menu.Item>


    <Menu.Item key=" global">
    <a rel="noopener noreferrer" href="#">
    Vue Globale
    </a>
    </Menu.Item>
    <Menu.Item danger>Déconnexion</Menu.Item>
    </Menu>
);

