import React from 'react';
import Layout, { Content, Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import {LeftMenu} from "./partial/LeftMenu";
import './assets/style.css';
import {Devis} from '../../module/devis/devis';
import {Overview} from '../../module/overview';
import {Tache} from '../../module/Tache/tache';
import {AddTache} from '../../module/Tache/addTache';
import {Mydocs} from '../../module/mydocs/mydocs';

import {Facture} from '../../module/facture/facture';
import * as axios from 'axios';

//import {Application} from '../../module/application';
import TopMenu from './partial/TopMenu';
import {Design} from '../../module/design/design';
import { Drawer } from 'antd';
import {NewMessage} from '../../module/message/NewSubject';
import { withAuth0,useAuth0 } from '@auth0/auth0-react';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    CaretRightOutlined
} from '@ant-design/icons';
import { 
    BrowserRouter as Router, 
    Route, 
    Switch, 
    useParams
} from 'react-router-dom'; 
//import { Conversation } from '../../module/message/conversation';
import { Reu } from '../../module/reunion/reunion';
import { DetailsTache } from '../../module/Tache/detailsTache';
import { Prestation } from '../../module/prestation/prestation';
import { Conversation } from '../../module/message/Conv';
import { Application } from './partial/application';
import { projectRoutes } from '../../../routes';

export const ProjectDashboardUI = ({collapse,projectId,project,path,client,userId}) => {
    
    return (
        <>                
        
        <Router>

            <Layout id="allLayout">

                {/*  left menu */}

                <Sider  trigger={null} collapsible collapsed={collapse.collaspeMenu}>
                    <LeftMenu projectId={projectId} userId={userId} toggle={collapse.toggle} 
                    collapse={collapse.collaspeMenu}
                    />
                </Sider>

                <Layout className="site-layout">
                {/*  HEADER */}
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                            <div className="headerLeft">
                            {React.createElement(collapse.collaspeMenu ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: collapse.toggle,
                            })}

                                <h1> {project?.data?.name}  <CaretRightOutlined /> {path}</h1>
                            </div>
                        <TopMenu client={client} toggle={this?.toggle} showDrawer={this?.showDrawer} {... this?.props}/>
                    
                    </Header>
                {/* BODY */}
                    <Content className="site-layout-background"  >   

                    <Switch>
                        {projectRoutes.map(
                            (route,index)=>(
                                <Route
                                key={index}
                                path={route.path}
                                exact={route.isExact}
                                render={(props)=><route.component {...props}/>}
                                ></Route>
                        ))}
                </Switch>
                    </Content>
                    </Layout>
                        
            </Layout>
            </Router>
        </>
    )
}

export default ProjectDashboardUI
