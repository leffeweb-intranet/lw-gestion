import React from 'react';
import Layout, { Content, Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import {LeftMenu} from "./partial/LeftMenu";
import './assets/style.css';
import {Devis} from '../../module/devis/devis';
import {Overview} from '../../module/overview';
import {Tache} from '../../module/Tache/tache';
import {AddTache} from '../../module/Tache/addTache';
import {Mydocs} from '../../module/mydocs/mydocs';

import {Facture} from '../../module/facture/facture';
import * as axios from 'axios';

//import {Application} from '../../module/application';
import TopMenu from './partial/TopMenu';
import {Design} from '../../module/design/design';
import { Drawer } from 'antd';
import {NewMessage} from '../../module/message/NewSubject';
import { withAuth0,useAuth0 } from '@auth0/auth0-react';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    CaretRightOutlined
} from '@ant-design/icons';
import { 
    BrowserRouter as Router, 
    Route, 
    Switch 
} from 'react-router-dom'; 
//import { Conversation } from '../../module/message/conversation';
import { Reu } from '../../module/reunion/reunion';
import { DetailsTache } from '../../module/Tache/detailsTache';
import { Prestation } from '../../module/prestation/prestation';
import { Conversation } from '../../module/message/Conv';
import { Application } from './partial/application';
import { projectRoutes } from '../../../routes';
//import { Conversation } from '../../module/message/conversation';

 class ProjectDashboard extends React.Component{

//     constructor(props){
//         super(props);
//         this.state = {
//             collapsed: false,
//             helper: false,
//             selection:'Overview',
//             client:{},
//             url:{
//                 overview:`${this.props.match.url}/overview`,
//                 tache:`${this.props.match.url}/tache`,
//                 addTache:`${this.props.match.url}/tache/add`,
//                 devis:`${this.props.match.url}/devis`,
//                 facture:`${this.props.match.url}/facture`,
//                 design:`${this.props.match.url}/design`,
//                 application:`${this.props.match.url}/application`,
//                 newMessage :`${this.props.match.url}/new-subject`,
//                 conversation:`${this.props.match.url}/conversation`,
//                 reunion:`${this.props.match.url}/reunion`,
//                 prestation:`${this.props.match.url}/prestation`,
//                 mydocs:`${this.props.match.url}/docs`,
                

//                 },
//             project:{}
//         };
//        this.getProject();
//     }

    
//   toggle = () => {
//     this.setState({
//       collapsed: !this.state.collapsed,
//     });
//   };
//   setSelection =(e) =>{
//       this.setState({
//           selection:e
//       });
//      // // // // console.log(this.state.selection);
//   }
//    showDrawer = () => {
//     this.setState({
//         helper:true
//     });
//   };
//    onClose = () => {
//     this.setState({
//         helper:false
//     });  };


//     getProject= async ()=>{
//         try {
//             const token = await this.props.auth0.getAccessTokenSilently({
//                 audience: 'http://localhost:5000/api',
//                 scope: 'read:project',
//             });


//             const result = await fetch(`http://localhost:5000/api/client/${this.props.auth0.user.sub}`, {
//                 method: 'GET',
//               headers: {
//                 authorization: `Bearer ${token}`
//               }
//             });
//             const data = await result.json();
//             // // // console.log(data);
//             this.setState({client:data.client});

//             const config = {
//                 headers: { Authorization: `Bearer ${token}` }
//             };
//             const project = await axios.post(`http://localhost:5000/api/project/`,{ id:this.props.match.params.id},config);

//             // // // console.log('project');
//             this.setState({project:project.data});

//         } catch (error) {
//             // // // console.log(error);
//         }
     
    
// }
    render() {
      //  // // // console.log(myUrl);
        // let id=this.props.match.params.id;
        // // // console.log(id);
        // const url = this.state.url;
        // // // console.log(id);
       // // // // console.log(url.OneTache);
        // let project=this.state.project;
        return (
            <>
            <Router>
                    <Layout id="allLayout">
                {
                        <Sider  trigger={null} collapsible collapsed={this.state.collapsed}>
                            <LeftMenu toggle={this.toggle}  setSelection={this.setSelection} {... this.state}/>

                        </Sider>}

                        <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        <div className="headerLeft">
                        {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: this.toggle,
                        })}

                        <h1> Project name{/*project ? project?.name :""*/} <CaretRightOutlined /> {this?.state?.selection}</h1>
                        </div>
                    <TopMenu toggle={this?.toggle} showDrawer={this?.showDrawer} {... this?.props}/>
                
                    </Header>

    
                    <Content className="site-layout-background"  >   

                    <Drawer
                    title="Aide"
                    placement="right"
                    closable={false}
                    onClose={()=>this.onClose()}
                    visible={this.state.helper}
                    >
                    <HelperRigth />

                    </Drawer>
                    <Switch>

                  {  projectRoutes.map((route,index)=> (
                        <route.component {...route} key={index}/>
                                ))}
                    {/* <Route exact path={url.overview} render={(props) => <Overview project={project}  {...props} /> } />
                        <Route  path={url.devis} render={(props) => <Devis  {...props} /> } />
                        <Route  path={url.facture} render={(props) => <Facture  {...props} /> } />
                        <Route  exact path={url.tache} render={(props) => <Tache  {...this.state} /> } />
                        <Route  exact path={url.addTache} render={(props) => <AddTache  {...props} /> } />
                        <Route  path={url.design} render={(props) => <Design  {...props} /> } />
                        <Route  path={url.newMessage} render={(props) => <NewMessage  {...props} /> } />
                        <Route  path={url.conversation} render={(props) => <Conversation/> }/>
                        <Route  path={url.reunion} render={(props) => <Reu  {...props} /> } />
                        <Route  exact path={`${url.tache}/:id`}   render={(props) => <DetailsTache  {...props} /> } />
                        <Route  path={url.prestation} render={(props) => <Prestation  {...props} /> } />
                        <Route  path={url.mydocs} render={(props) => <Mydocs />}  />
                        <Route  path={url.application} render={(props) => <Application />}  /> */}

                        
                    </Switch>
            
                    </Content>
                    
                    </Layout>
                </Layout>
                </Router>
            </>
        )
    }


}


const HelperRigth =({content})=>{
    return(
        <h1>Aide</h1>
    )
}

export default withAuth0(ProjectDashboard);