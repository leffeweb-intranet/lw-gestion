import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { Card, Col, Dropdown, Menu, Row } from 'antd';
import Layout, { Content } from 'antd/lib/layout/layout';
import { NavLink } from 'react-router-dom';
import { Logo } from '../../module/Logo';
import './style.css';

export default function Users({users,user,client,logout}){
    return(

        <Layout id="allLayout">
        <Row>
            <Col
             className='headerLW' span={24}>
                <Row justify="space-between">
                <Col span={2} >
                    <Logo noPadding/>
                </Col>
                <Col span={3}>
                    <MenuRight client={client} logout={logout} client={{}}/>
                    
                </Col>
                </Row>
            </Col>
            
            </Row>
            <Row>

            </Row>
            <Layout className="site-layout">

            <Content >
                <Card title={"Utilisateurs"}>
                    <Row gutter={[16,16]}>
                {   
                users.data.map(user=>{
                    return   <Item user={user}/>
                    })}
                    </Row>
                    </Card>
            </Content>
            </Layout>
            </Layout>
        
    );

}



const MenuRight =  ({match,logout,client})=>{
    console.log("client",client);
    return(
        <Dropdown overlay={<Drop logout={logout}/>} >
    
        <a className="topMenu" onClick={e => e.preventDefault()}>
    
        <UserOutlined />
         {client.data?.firstName } {client.data?.lastName } <DownOutlined />
        </a>
      </Dropdown>
    )
}

  
    const Drop= ({logout})=>{
    return(   
        <Menu>
        <Menu.Item>
        <a rel="noopener noreferrer" href="#">
            Mes informations
        </a>
        </Menu.Item>
    
    
        <Menu.Item>
        <a rel="noopener noreferrer" href="#">
        Vue Globale
        </a>
        </Menu.Item>
        <Menu.Item danger onClick={logout}>Déconnexion</Menu.Item>
        </Menu>
    )
    }


    const Item=({user})=>{
        console.log(user);
        return(
            <Card>
                <NavLink to={`/user/projects/${user.id}`}>
                    <h3>{user.firstName} {user.lastName}</h3>
                </NavLink>
        </Card>

        )
    }