import React from 'react'
import routes from './routes'
import { Loading } from './Loading';
import { useAuth0 } from '@auth0/auth0-react';
import { ErrorDist } from './Erreur';
import "./App.css"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import { CaretLeftOutlined, LeftOutlined } from '@ant-design/icons';

const RenderRoute = (route) => {
  return (
      <Route
        path={route.path}
        exact
        render={
          (props)=>
          <>
          <route.component {...props}/></>
        }
        ></Route>
        )
}


export const App = (props) => {

    const {
        isLoading,
        isAuthenticated,
        error,
        user,
        loginWithRedirect,
        logout,
      } = useAuth0();

      const history=useHistory();
      console.log("isAuthenticated",isAuthenticated);
      if (isLoading) {
        return <Loading/>;
      }
      if (error) {
        return <ErrorDist/>;
      }
      
      if (isAuthenticated) {
        return (
            <>
          
                <Router>
                  <Switch>
                    {routes.map((route,index)=> (
                      <Route
                      key={index}
                      path={route.path}
                      exact={route.isExact}
                      render={(props)=><route.component {...props}/>}
                      ></Route>
                    ))}
                  </Switch>
                </Router>

                    </>
        );
      } else {
        loginWithRedirect()
    }
    
}

