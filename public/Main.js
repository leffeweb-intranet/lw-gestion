const {app, BrowserWindow} = require('electron')    

function createWindow () {   
  // Create the browser window.     
  let win = new BrowserWindow({width: 800, height: 600,titleBarStyle: 'hiddenInset' }) 


  win.setMenu(null);

  // and load the index.html of the app.     
  win.loadURL('http://localhost:3001/');
}      

app.on('ready', createWindow)
